<?php

namespace Ratauto\ExtendMeta;

use Event;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name' => 'Extend Meta Plugin',
            'description' => 'Add Meta Image field',
            'author' => 'Algirdas Tamasauskas',
        ];
    }

    public function register()
    {
        Event::listen('backend.form.extendFields', function ($widget) {
            $canAdd = false;
            if (isset($widget->tabs['cssClass'])) {
                if ('master-area' == $widget->tabs['cssClass']) {
                    if ($widget->model instanceof \RainLab\Pages\Classes\Page) {
                        $widget->addFields([
                            'settings[meta_image]' => [
                                'label' => 'Meta Image',
                                'tab' => 'cms::lang.editor.meta',
                                'span' => 'left',
                                'type' => 'mediafinder',
                                'mode' => 'image',
                            ],
                        ], 'primary');
                    }
                }
            }
        });
    }
}
